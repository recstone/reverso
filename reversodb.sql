-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 07 juin 2020 à 13:50
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `reversodb`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `raison sociale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero rue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom rue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code postal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chiffre affaire` int(11) NOT NULL,
  `nombre employe` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`ID`, `raison sociale`, `numero rue`, `nom rue`, `code postal`, `ville`, `telephone`, `mail`, `commentaire`, `chiffre affaire`, `nombre employe`) VALUES
(1, 'Ferrari', '43', 'boulevard le soif', '86001', 'Milan', '0658227948', 'ferrari@mail.com', ' ', 120000000, 6000),
(2, 'Hyundai', '30', 'rue de Miyazaki', '45561', 'Okinawa', '0243336578', 'hyundai@mail.com', ' ', 3000000, 2744),
(3, 'Mitsubishi', '2', 'impasse Dorin', '10237', 'Tokyo', '0692821324', 'mitsubishi@mail.com', ' ', 500000, 4325),
(4, 'Renault', '18', 'avenue du repos', '75016', 'Paris', '0619698345', 'renault@mail.com', ' ', 35000000, 4000);

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_contratClient` int(11) NOT NULL,
  `libelle contrat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant contrat` int(11) NOT NULL,
  `date debut contrat` date NOT NULL,
  `date fin contrat` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `id_contratClient` (`id_contratClient`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`ID`, `id_contratClient`, `libelle contrat`, `montant contrat`, `date debut contrat`, `date fin contrat`) VALUES
(1, 0, 'contrat 1 client 1', 1400, '2017-05-10', '2019-09-02'),
(2, 0, 'contrat 1 client 2', 2700, '2004-03-10', '2009-02-16'),
(3, 0, 'contrat 2 client 1', 8000, '2015-12-03', '2017-08-12'),
(4, 0, 'contrat 2 client 2', 900, '2018-03-14', '2022-07-05'),
(5, 0, 'contrat 1 client 3', 10200, '2012-05-10', '2016-09-02'),
(6, 0, 'contrat 2 client 3', 500, '2004-05-10', '2007-09-02'),
(7, 0, 'contrat 1 client 4', 27000, '2001-01-02', '2001-06-18'),
(8, 0, 'contrat 2 client 4', 32000, '2017-05-10', '2019-09-02');

-- --------------------------------------------------------

--
-- Structure de la table `prospect`
--

DROP TABLE IF EXISTS `prospect`;
CREATE TABLE IF NOT EXISTS `prospect` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `raison sociale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero rue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom rue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code postal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date prospection` date NOT NULL,
  `interet prospect` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `prospect`
--

INSERT INTO `prospect` (`ID`, `raison sociale`, `numero rue`, `nom rue`, `code postal`, `ville`, `telephone`, `mail`, `commentaire`, `date prospection`, `interet prospect`) VALUES
(1, 'jaguar', '12', 'allée des myosotis', '97400', 'texas', '0262306524', 'jaguar@mail.com', NULL, '1989-04-19', 'oui'),
(2, 'porsche', '6', 'rue des jonquilles', '45955', 'rome', '0262410593', 'porsche@mail.com', NULL, '2018-08-12', 'non'),
(3, 'subaru', '43', 'boulevard du saint pierrois', '35500', 'kyoto', '0624306573', 'subaru@mail.com', NULL, '2010-07-05', 'non'),
(4, 'nissan', '29 bis', 'rue saint julien', '54000', 'nancy', '0624566774', 'nissan@mail.com', 'sympa mais chauve', '2005-03-14', 'oui');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
