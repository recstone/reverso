/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFPA.CDA03.reverso;

import java.time.LocalDate;
import metiers.Prospect;
import metiers.Societe;
import exception.ExceptionMetier;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import utilitaire.Utilitaire;

/**
 *
 * @author Alexandre
*/


@DisplayName("classe test societe")
public class SocieteTest extends Societe {
    
    public SocieteTest() throws ExceptionMetier {}
   
    @Test
    void null_RsSociete() throws ExceptionMetier
    {
        Prospect prospect = new Prospect();
        
        boolean exceptionRaised = false;
        try {
            prospect.setRaisonSociale(null);
        }
        catch (ExceptionMetier re) {
            exceptionRaised = true;
        }
        
        assertTrue(exceptionRaised);
    }

    
    @ParameterizedTest
    @ValueSource(strings = {"sylvie.ici@afpa.com", "sylvieici@afpa.com" })         
    void testPatternMAil(String mail) {
        assertTrue(Utilitaire.PATTERNMAIL.matcher(mail).matches());
    }
    
    @Test
    void valueNullEmptySetterRaisonSociale () {
        String[] invalidValues = {"", null, "\r", "\n" };
        assertTrue( true );
        for (String invalidValue : invalidValues) {
           assertThrows(exception.ExceptionMetier.class, () -> { new SocieteTest().setRaisonSociale(invalidValue); }); 
       }
    }
    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"Alexandre", "Recstone", "rectone.gmail.com","alexandre.colomar54@gmail.com", "" })
    void invalidFormatMail (String invalidValues) throws ExceptionMetier {
        assertThrows(exception.ExceptionMetier.class, () -> { new SocieteTest().setMail(invalidValues); }); 
    }
    
    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"03833333", "00", "06060606","1000000000", "0000000000", "+325333333333" })
    void invalidFormatTel (String invalidValues) throws ExceptionMetier {
        assertThrows(exception.ExceptionMetier.class, () -> { new SocieteTest().setTelephone(invalidValues); }); 
    }
    
    @Test
    @NullSource
    @DisplayName("tests sur valeurs null ou vides")        
    void valuesNullSetters() {
        assertThrows(exception.ExceptionMetier.class, () -> { new SocieteTest().setRaisonSociale(""); }); 
      //  assertThrows(metier.exceptions.MetiersException.class, () -> { new SocieteTest().setTypeSociete(""); }); 
       
  }
    
    @ParameterizedTest
    @ValueSource(strings = {"sylvie.ici@afpa", "sylvieici@afpa.com" })        
    void mailOk (String mail) {
        assertDoesNotThrow(
                () -> {
                    new SocieteTest().setMail(mail);
                });
    }
    @ParameterizedTest
    @ValueSource(strings = {"0303030303", "0033333333333","+33123456789"," +33123456789 ", "03 03 03 03 03", "04.04.04.04.04",
    "05/05/05/05/05", "+336-06-06-06-06"})        
    void telOk (String tel) {
        assertDoesNotThrow(
                () -> {
                    new SocieteTest().setTelephone(tel);
                });
    }
    @ParameterizedTest
    @ValueSource(strings = { "01.01.2017", "31.12.2017" })
    void testWithExplicitJavaTimeConverter(
            @JavaTimeConversionPattern("dd.MM.yyyy") LocalDate argument) {
        assertEquals(2017, argument.getYear());
    }

    
}

