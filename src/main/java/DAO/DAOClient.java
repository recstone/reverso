/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.ConnexionManager;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import metiers.Client;


/**
 *
 * @author alexa
 */
public class DAOClient extends ConnexionManager {
   
    

    static ConnexionManager conManager = new ConnexionManager();

   
  
    
        public static void findAllClient()
        throws SQLException {

        Statement stmt = null;
        String query = "select * from reversodb.client";
        
        try {
            
            Connection con = conManager.getConnection();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {

                int clientID = rs.getInt("ID");
                String raisonSociale = rs.getString("raison sociale");
                String numeroRue = rs.getString("numero rue");
                String nomRue = rs.getString("nom rue");
                String codePostal = rs.getString("code postal");
                String ville = rs.getString("ville");
                String telephone = rs.getString("telephone");
                String mail = rs.getString("mail");
                String commentaire = rs.getString("commentaire");
                int chiffreAffaire = rs.getInt("chiffre affaire");
                int nombreEmploye = rs.getInt("nombre employe");

            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch(IOException ioe) 
        {
            ioe.printStackTrace();
        }
            finally {
            if (stmt != null) { stmt.close(); }
        }
    }

        public void findClient(String client)
        throws SQLException {

        Statement stmt = null;
        String query = "select id, raison sociale, numero rue, nom rue, code postal,"
                + "     ville, telephone, mail, commentaire,chiffre affaire, nombre employe from reversodb where client =" + client;
        
        try {
            Connection con = conManager.getConnection();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {

                int clientID = rs.getInt("ID");
                String raisonSociale = rs.getString("raison sociale");
                String numeroRue = rs.getString("numero rue");
                String nomRue = rs.getString("nom rue");
                String codePostal = rs.getString("code postal");
                String ville = rs.getString("ville");
                String telephone = rs.getString("telephone");
                String mail = rs.getString("mail");
                String commentaire = rs.getString("commentaire");
                int chiffreAffaire = rs.getInt("chiffre affaire");
                int nombreEmploye = rs.getInt("nombre employe");
               
            }
        } catch (SQLException sqle ) {
            sqle.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();       
        } finally {
            if (stmt != null) { stmt.close(); }
        }
    }
        
        

        public void createClient(String raisonSociale, 
                                String numeroRue, 
                                String nomRue, 
                                String codePostal,
                                String ville,
                                String telephone, 
                                String mail, 
                                String commentaire, 
                                int chiffreAffaire, 
                                int nombreEmploye) 
        throws SQLException, IOException {
            
            
        
        Statement preStmt = null;
        String query = "INSERT INTO client VALUES ("+raisonSociale+",'"
                                                    +numeroRue+",'"
                                                    +nomRue+",'"
                                                    +codePostal+",'"
                                                    +ville+",'"
                                                    +telephone+",'"
                                                    +mail+",'"
                                                    +commentaire+"',"
                                                    +chiffreAffaire+",'"
                                                    +nombreEmploye+")";
        
            Connection con = conManager.getConnection();
            preStmt = con.createStatement();
            ResultSet rs = preStmt.executeQuery(query);
      
       } 
        
        
        
        
        public void deleteClient(Client client ) 
        throws SQLException, IOException {
            
            
        Connection con = conManager.getConnection();
        Statement stmt = con.createStatement();
        
        PreparedStatement deleteClient = con.prepareStatement(" DELETE * FROM `client` WHERE `client`.`raison sociale` = "+client+"");
        }     
         
       /* public void saveClient(Client client, String colonne, String nouvelleValeur, int  id) 
        throws SQLException, IOException {
            
            
        Connection con = conManager.getConnection();
        Statement stmt = con.createStatement();
        
        PreparedStatement saveClient = con.prepareStatement(" UPDATE "+ client+" SET "+colonne+" = "+nouvelleValeur+" WHERE `client`.`ID` = "+id+"");
        } */
        
        public static void saveClient(Client client) throws SQLException, IOException{
            try {
                Connection con = conManager.getConnection();
                
                if(client.getRaisonSociale() != null){
                    Statement statement = con.createStatement();
                    statement.executeQuery("update client set raison sociale ="+client.getRaisonSociale()+" where id = "+client.getIdSociete());
                }
                
            } catch (SQLException e){
                
              e.printStackTrace(); 
            }
            
        }
}
    
    
 
     

