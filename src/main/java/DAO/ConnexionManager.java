/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.sql.SQLException;
import java.util.logging.LogManager;
import java.util.logging.Logger;



public class ConnexionManager  {
   



public Connection getConnection() throws SQLException, IOException {

   
    
    Connection connection = null;
    Properties dataProperties = new Properties();
    

    try{
       dataProperties.load(getClass().getClassLoader().getResourceAsStream("dataBase.properties"));
      connection = DriverManager.getConnection(
                    dataProperties.getProperty("url"),
                    dataProperties.getProperty("login"),
                    dataProperties.getProperty("password"));
          System.out.println("Connected to database");
    
        return connection;    
      
    } catch (SQLException e){
        e.printStackTrace();
    } catch (IOException io){
        io.printStackTrace();
        return null;
    }
        
    
  
    
        return connection;    
    
}
}

/*public static Singleton getInstance(){

if(connection == null){

connection = new Singleton();
}
}*/