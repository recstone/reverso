/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author alexa
 */
public class DAOProspect {
    
   public static void findAll(Connection con, String reverso)
    throws SQLException {

    Statement stmt = null;
    String query = "select id, raison sociale, numero rue, nom rue, code postal,"
            + "     ville, telephone, mail, commentaire,date prospection, interet prospect from " + reverso + ".prospect";
    try {
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            
            int prospectID = rs.getInt("ID");
            String raisonSociale = rs.getString("raison sociale");
            String numeroRue = rs.getString("numero rue");
            String nomRue = rs.getString("nom rue");
            String codePostal = rs.getString("code postal");
            String ville = rs.getString("ville");
            String telephone = rs.getString("telephone");
            String mail = rs.getString("mail");
            String commentaire = rs.getString("commentaire");
            Date dateProspection = rs.getDate("chiffre affaire");
            String interet = rs.getString("interet prospect");
            /*System.out.println(coffeeName + "\t" + supplierID +
                               "\t" + price + "\t" + sales +
                               "\t" + total);*/
        }
    } catch (SQLException e ) {
        e.printStackTrace();
    } finally {
        if (stmt != null) { stmt.close(); }
    }
}  
   
      public static void find(Connection con, String reverso, String prospect)
    throws SQLException {

    Statement stmt = null;
    String query = "select id, raison sociale, numero rue, nom rue, code postal,"
            + "     ville, telephone, mail, commentaire,date prospection, interet prospect from " + reverso + "where prospect =" + prospect;
    try {
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            
            int prospectID = rs.getInt("ID");
            String raisonSociale = rs.getString("raison sociale");
            String numeroRue = rs.getString("numero rue");
            String nomRue = rs.getString("nom rue");
            String codePostal = rs.getString("code postal");
            String ville = rs.getString("ville");
            String telephone = rs.getString("telephone");
            String mail = rs.getString("mail");
            String commentaire = rs.getString("commentaire");
            Date dateProspection = rs.getDate("chiffre affaire");
            String interet = rs.getString("interet prospect");
            /*System.out.println(coffeeName + "\t" + supplierID +
                               "\t" + price + "\t" + sales +
                               "\t" + total);*/
        }
    } catch (SQLException e ) {
        e.printStackTrace();
    } finally {
        if (stmt != null) { stmt.close(); }
    }
}  
    
}
