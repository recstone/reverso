package AFPA.CDA03.reverso;

import exception.ExceptionMetier;
import java.time.LocalDate;
import java.util.Collections;
import javax.swing.JOptionPane;
import jframe.Accueil;
import metiers.Client;
import metiers.Contrat;
import metiers.Prospect;
import utilitaire.CollectionClient;
import utilitaire.CollectionProspect;
import static utilitaire.CollectionSociete.ajouter;
import DAO.ConnexionManager;
import DAO.DAOProspect;
import DAO.DAOClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{

          
     /**
     * @param args the command line arguments
     * @throws exception.ExceptionMetier
     */
     
    public static void main(String[] args) throws ExceptionMetier, SQLException, IOException {
        
        
   DAO.DAOClient.findAllClient();
        
        
    try {  
    //LISTE CONTRAT
    
    Contrat contrat1 = new Contrat ("contrat1 client1", 1400.15, LocalDate.of(2017,5,10),  LocalDate.of(2019,9,2));
    Contrat contrat2 = new Contrat ("contrat2 client1", 2700.15, LocalDate.of(2004,3,10),  LocalDate.of(2009,2,16));
    Contrat contrat3 = new Contrat ("contrat1 client2", 8000.15, LocalDate.of(2015,12,03),  LocalDate.of(2017,8,12));
    Contrat contrat4 = new Contrat ("contrat2 client2", 900.15, LocalDate.of(2018,3,14),  LocalDate.of(2022,7,5));
    Contrat contrat5 = new Contrat ("contrat1 client3", 10200.15, LocalDate.of(2012,5,10),  LocalDate.of(2016,9,2));
    Contrat contrat6 = new Contrat ("contrat2 client3", 500.15, LocalDate.of(2004,5,10),  LocalDate.of(2007,9,2));
    Contrat contrat7 = new Contrat ("contrat1 client4", 27000.15, LocalDate.of(2001,1,02),  LocalDate.of(2001,6,18));
    Contrat contrat8 = new Contrat ("contrat2 client4", 32000.15, LocalDate.of(2017,5,10),  LocalDate.of(2019,9,2));
    

        
    //LISTE CLIENT
    
    //String raisonSociale, int numeroRue, String nomRue, int codePostal, String ville, String telephone, String mail, int chiffreAffaire, int nbEmploye, 
    Client client2 = new Client("Hyundai", "30", "rue de Miyazaki", "45561", "Okinawa", "0243336578", "hyundai@mail.com","", 3000000, 2744);
    Client client3 = new Client("Mitsubishi", "2", "impasse Dorin", "10237", "Tokyo", "0692821324", "mitsubishi@mail.com","", 500000, 4325);
    Client client4 = new Client("Renault", "18", "avenue du repos", "75016", "Paris", "0619698345", "renault@mail.com","" , 35000000, 4000);
    Client client1 = new Client("Ferrari", "43", "boulevard le soif", "86001", "Milan", "0658227948", "ferrari@mail.com","" , 120000000, 6000);
    
    
    client1.ajouterContrat(contrat1);
    client1.ajouterContrat(contrat2);
    client2.ajouterContrat(contrat3);
    client2.ajouterContrat(contrat4);
    client3.ajouterContrat(contrat5);
    client3.ajouterContrat(contrat6);
    client4.ajouterContrat(contrat7);
    client4.ajouterContrat(contrat8);
    
    //LISTE PROSPECT
    //String raisonSociale, int numeroRue, String nomRue, int codePostal, String ville, String telephone, String mail, String dateProspection, String prospectInterest)
    Prospect prospect1 = new Prospect("Jaguar", "12", "allée des myosotis", "97400", "Texas", "0262306524", "jaguar@mail.com","" , "19/04/1989", "oui");
    Prospect prospect2 = new Prospect("Porsche", "6", "rue des jonquilles", "45955", "Rome", "0262410593", "porsche@mail.com","" , "12/08/2018", "non");
    Prospect prospect3 = new Prospect("Subaru", "43", "boulebard du saint pierrois", "35500", "Kyoto", "0624306573", "subaru@mail.com","" , "14/03/1989", "non");
    Prospect prospect4 = new Prospect("Nissan", "29 bis", "rue Saint Julien", "54000", "Nancy", "0624566774", "nissan@mail.com","" , "10/07/2023", "oui");
    
    //AJOUT
    ajouter(client1);
    ajouter(client2);
    ajouter(client3);
    ajouter(client4);
    ajouter(prospect1);
    ajouter(prospect2);
    ajouter(prospect3);
    ajouter(prospect4); 
    
    //TRI
    Collections.sort(CollectionProspect.getListeProspect(), CollectionProspect.triListe);
    Collections.sort(CollectionClient.getListeClient(), CollectionClient.triListe);
    

      Accueil accueil = new Accueil();
      accueil.setVisible(true);
      
      //ajouterClient(conn, "Hyundai", "30", "rue de Miyazaki", "45561", "Okinawa", "0243336578", "hyundai@mail.com","", 3000000, 2744);
     
      
        
    } catch(ExceptionMetier e)  {
        System.out.println(e.getMessage());
        
        //JOptionPane.showMessageDialog(null, e.getMessage(), "erreur", 0);
    } 

    }  
}