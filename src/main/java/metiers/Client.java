/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metiers;
import exception.ExceptionMetier;
import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import utilitaire.*;
import DAO.ConnexionManager;
/**
 *
 * @author CDA03-02
 */
public  class Client extends Societe {
    
    //ATTRIBUTS
    private int chiffreAffaire;
    private int nbEmploye;
    private  ArrayList<Contrat> listeContrat;

    
    private int nbContrats;
    private float montantContrats;
    
    //CONSTRUCTEURS
    /**
     * Constructeur de la classe Client héritant de Societe
     * @param raisonSociale
     * @param numeroRue
     * @param nomRue
     * @param codePostal
     * @param ville
     * @param telephone
     * @param mail
     * @param chiffreAffaire
     * @param nbEmploye
     * @param commentaire
     * @param listeContrat
     * @throws ExceptionMetier 
     */
    
    
    
    public Client(String raisonSociale, String numeroRue, String nomRue, String codePostal, String ville, 
            String telephone, String mail,String commentaire, int chiffreAffaire, int nbEmploye) throws ExceptionMetier {
        super(raisonSociale, numeroRue, nomRue, codePostal, ville, telephone, mail, commentaire);
        setChiffreAffaire(chiffreAffaire);
        setNbEmploye(nbEmploye);
        listeContrat = new ArrayList<Contrat>();
        
    }    
       
    //GETTER ET SETTER

    public int getChiffreAffaire() {
        return chiffreAffaire;
    }
    public void setChiffreAffaire(int chiffreAffaire) throws ExceptionMetier {
        if(chiffreAffaire  >= 200){
            this.chiffreAffaire = chiffreAffaire;
        } else {
            throw new ExceptionMetier("Le chiffre d'affaire doit eter superieur ou egal a 200");
        }
        
    }
    public int getNbEmploye() {
        return nbEmploye;
    }   
    public void setNbEmploye(int nbEmploye) throws ExceptionMetier{
        String nbEmployeParse = Integer.toString(nbEmploye);
        
        if(nbEmploye <= 0 || nbEmployeParse.isEmpty() ){
            throw new ExceptionMetier("Le nombre d'employé ne peut être inférieur a zero");
        } else {
           this.nbEmploye = nbEmploye;
        }   
    }
    

    public int getNbContrats() {
        return nbContrats;
    }
    public void setNbContrats(int p_nbContrats) throws ExceptionMetier {
      
            this.nbContrats = p_nbContrats;
   
    }
    public float getMontantContrats() {
        return montantContrats;
    }
    public void setMontantContrats(int montantContrats) throws ExceptionMetier {
      
            this.montantContrats = montantContrats;
   
    }
    
    

    //METHODES

    public static void afficher(){
        
        int occurenceClient = 0;

        
        for (int i = 0; i < CollectionClient.getListeClient().size(); i++) {
            System.out.println(CollectionClient.getListeClient().get(i));
            occurenceClient++;
        }
        System.out.println("Nombre de clients: " + occurenceClient);
    }
    
    
    
    public void ajouterContrat(Contrat contrat){   
         this.listeContrat.add(contrat); 
    }
    

      public   ArrayList<Contrat> getListeContrat() {
        return listeContrat;
    }  

  
}

    
    
    
    

