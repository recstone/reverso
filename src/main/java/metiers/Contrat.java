/*
 Compléter votre projet Réverso en y ajoutant une nouvelle classe Contrat avec les attributs suivants : 
    int idContrat
    String libelleContrat
    double montantContrat​
    LocalDate dateDebutContrat
    LocalDate dateFinContrat
*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metiers;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author alexa
 */
public class Contrat {

//ATTRIBUTS DE CLASSES
    private static int ID = 1;
    private int idContrat;
    private String libelleContrat;
    private double montantContrat;
    private LocalDate dateDebutContrat;
    private LocalDate dateFinContrat;
    
//CONSTRUCTEUR
    public Contrat(String p_libelleContrat, double p_montantContrat, LocalDate p_dateDebutContrat, LocalDate p_dateFinContrat)
    {
       setIdContrat();
       setLibelleContrat(p_libelleContrat);
       setMontantContrat(p_montantContrat);
       setDateDebutContrat(p_dateDebutContrat);
       setDateFinContrat(p_dateFinContrat);
    }
    
//SETTER ET GETTER
    public int getIdContrat() {
        return idContrat;
    }
    public void setIdContrat() {
        idContrat = ID++;
    }

    
    public String getLibelleContrat() {
        return libelleContrat;
    }
    public void setLibelleContrat(String libelleContrat) {
        this.libelleContrat = libelleContrat;
    }
    public double getMontantContrat() {
        return montantContrat;
    }
    public void setMontantContrat(double montantContrat) {
        this.montantContrat = montantContrat;
    }
    public LocalDate getDateDebutContrat() {
        return dateDebutContrat;
    }
    public void setDateDebutContrat(LocalDate dateDebutContrat) {
        this.dateDebutContrat = dateDebutContrat;
    }
    public LocalDate getDateFinContrat() {
        return dateFinContrat;
    }
    public void setDateFinContrat(LocalDate dateFinContrat) {
        this.dateFinContrat = dateFinContrat;
    }
    

    
}
